# rcstring

rcstring is a dependency-free implementation of a subset of features available
in `std::ffi::CString`. This enables developers writing things like drivers,
kernels or operating systems without libstd to have a clean CString API
without re-inventing the wheel.
